package mongo;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.MongoClient;
import java.util.Set;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author ahmet.mungen
 */
public class test {

    public static void main(String[] args) {

        try {
            MongoClient mongo = new MongoClient("localhost", 27017);
            DB db = mongo.getDB("ahmet");
            Set<String> tables = db.getCollectionNames();
            DBCollection table = db.getCollection("ahmet");

//INSERT
            BasicDBObject document = new BasicDBObject();
            document.put("name", "mkyong");
            document.put("age", 30);
            //document.put("createdDate", new Date());
            //table.insert(document);

            //UPDATE
            BasicDBObject query = new BasicDBObject();
            query.put("name", "mkyong");
            BasicDBObject newDocument = new BasicDBObject();
            newDocument.put("name", "mkyong-updated");
            BasicDBObject updateObj = new BasicDBObject();
            updateObj.put("$set", newDocument);
            table.update(query, updateObj);

            //FİND
            BasicDBObject searchQuery = new BasicDBObject();
            searchQuery.put("name", "mkyong-updated");
            DBCursor cursor = table.find(searchQuery);
            while (cursor.hasNext()) {
                System.out.println(cursor.next());
            }

          //  for (String coll : tables) {
            //      System.out.println(coll);
            //   }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
