package mongo;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.MongoClient;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import org.apache.tomcat.util.codec.binary.Base64;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author ahmet.mungen
 */
public class MongoProcess {

    String dbUrl = "jdbc:mysql://localhost:3306/instagram?useUnicode=true&characterEncoding=UTF-8";
    String root = "root";
    String password = "";

    
    //String dbUrl = "jdbc:mysql://84.201.37.5:3306/instagram?useUnicode=true&characterEncoding=UTF-8";
    //String root = "ahmet";
    //String password = "ahmet1990";
    //String MongoURL = "84.201.37.5";
    
    
    String MongoURL = "localhost";
    int MongoPort = 27017;
    String MongoDBName = "instagram";

    public static void main(String[] args) {
        MongoProcess is = new MongoProcess();
        is.getDBtoMongoUser();
        // is.getDBtoMongoLikes();
        //is.isRecordedUsername("ahmetmungen");
        // is.deleteusers();
        // is.isRecordedLike("829749654236981594_238024889238024889286845449");
        // is.GetLikeSender("990241851");
        // is.JSONtoUser("");
//is.isRecordedUsername("ahmet");
        // is.JSONtoUser(" ");
        is.GetUserName("286015081");

    }

    public MongoProcess() {
    }
    String likeSingle = "{\n"
            + "   \"_id\":{\n"
            + "      \"$oid\":\"544e2a70cf9f265e5f881d22\"\n"
            + "   },\n"
            + "   \"id\":137,\n"
            + "   \"photo_id\":\"829749654236981594_238024889\",\n"
            + "   \"owner_id\":\"238024889\",\n"
            + "   \"sender_id\":\"286845449\",\n"
            + "   \"photoownersender\":\"829749654236981594_238024889238024889286845449\"\n"
            + "}";

    String userJSON = "{ \"_id\" : { \"$oid\" : \"544e353bcf9fb108a4c7d384\"} , \"id\" : 1 , \"userid\" : 990241851 , \"username\" : \"ahmetmungen\" , \"test\" : 1 , \"full_name\" : \"Ahmet Müngen\" , \"followed_by\" : 111 , \"follows\" : 123 , \"notpublic\" : 0 , \"media\" : 7}";

    public String LikeTableJSONtoSenderID(String JSONText) {

        // System.out.println("JSONText = " + JSONText);
        JSONParser parser = new JSONParser();
        String sender_id = "";
        try {

            Object obj = parser.parse(JSONText);
            JSONObject jsonObject = (JSONObject) obj;
            sender_id = (jsonObject.get("sender_id").toString());

        } catch (Exception e) {
            e.printStackTrace();
        }
        return sender_id;
    }

    public String UserTableJSONtoUsername(String JSONText) {

        // System.out.println("JSONText = " + JSONText);
        JSONParser parser = new JSONParser();
        String username = "";
        try {

            Object obj = parser.parse(JSONText);
            JSONObject jsonObject = (JSONObject) obj;
            username = (jsonObject.get("username").toString());
            //  System.out.println("username = " + username);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return username;
    }

    public String JSONtoUserID(String JSONText) {
        JSONText = userJSON;
        //   System.out.println("JSONText = " + JSONText);
        JSONParser parser = new JSONParser();
        String userid = "";
        try {

            Object obj = parser.parse(JSONText);
            JSONObject jsonObject = (JSONObject) obj;
            String id = (jsonObject.get("id").toString());
            userid = (jsonObject.get("userid").toString());
            String username = (jsonObject.get("username").toString());
            String test = (jsonObject.get("test").toString());
            String full_name = (jsonObject.get("full_name").toString());
            String followed_by = (jsonObject.get("followed_by").toString());
            String follows = (jsonObject.get("follows").toString());
            String notpublic = (jsonObject.get("notpublic").toString());
            String medi = (jsonObject.get("media").toString());

        } catch (Exception e) {
            e.printStackTrace();
        }
        return userid;
    }

    public void deleteusers() {
        try {

            MongoClient mongo = new MongoClient(MongoURL, MongoPort);
            DB db = mongo.getDB(MongoDBName);
            DBCollection table = db.getCollection("users");
            for (int i = 0; i < 50000; i++) {
                if (i % 1000 == 0) {
                    System.out.println("Sayac: " + i);
                }
                BasicDBObject searchQuery = new BasicDBObject();
                searchQuery.put("id", i);
                table.remove(searchQuery);
            }
            mongo.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void getDBtoMongoUser() {
        try {

            MongoClient mongo = new MongoClient(MongoURL, MongoPort);
            DB db = mongo.getDB(MongoDBName);
            DBCollection table = db.getCollection("users");

            Connection conn = null;
            Statement st = null;
            ResultSet rs = null;

            try {
                Class.forName("com.mysql.jdbc.Driver").newInstance();
                conn = DriverManager.getConnection(dbUrl, root, password);
                st = conn.createStatement();
                rs = st.executeQuery("Select * From `usertable`");
                int sayac = 0;
                while (rs.next()) {
                    sayac++;
                    if (sayac % 1000 == 0) {
                        System.out.println("Sayac: " + sayac);
                    }
                    if (!isRecordedUsername(rs.getString("username"))) {
                        System.out.println(rs.getString("username"));

                        BasicDBObject document = new BasicDBObject();
                        document.put("id", rs.getInt("id"));
                        document.put("userid", rs.getInt("userid"));
                        document.put("username", rs.getString("username"));
                        document.put("test", rs.getInt("test"));
                        document.put("full_name", rs.getString("full_name"));
                        document.put("followed_by", rs.getInt("followed_by"));
                        document.put("follows", rs.getInt("follows"));
                        document.put("notpublic", rs.getInt("notpublic"));
                        document.put("media", rs.getInt("media"));
                        table.insert(document);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            mongo.close();
        } catch (Exception e) {
        }
    }

    public boolean isRecordedUsername(String username) {
//by username
        try {

            MongoClient mongo = new MongoClient(MongoURL, MongoPort);
            DB db = mongo.getDB(MongoDBName);
            DBCollection table = db.getCollection("users");
            BasicDBObject searchQuery = new BasicDBObject();
            searchQuery.put("username", username);
            DBCursor cursor = table.find(searchQuery);
            while (cursor.hasNext()) {
                System.out.println(cursor.next());
                return true;
            }
            mongo.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean isRecordedLike(String photoownersender) {
        //by username
        try {

            MongoClient mongo = new MongoClient(MongoURL, MongoPort);
            DB db = mongo.getDB(MongoDBName);
            DBCollection table = db.getCollection("likes");
            BasicDBObject searchQuery = new BasicDBObject();
            searchQuery.put("photoownersender", photoownersender);
            DBCursor cursor = table.find(searchQuery);
            while (cursor.hasNext()) {
                //  System.out.println(cursor.next());
                return true;
            }
            mongo.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public ArrayList<String> GetLikeSender(String owner_id) {
        ArrayList<String> arkadaslist = new ArrayList<>();

        try {
            //by username
            MongoClient mongo = new MongoClient(MongoURL, MongoPort);
            DB db = mongo.getDB(MongoDBName);
            DBCollection table = db.getCollection("likes");
            BasicDBObject searchQuery = new BasicDBObject();
            searchQuery.put("owner_id", owner_id);
            DBCursor cursor = table.find(searchQuery);
            while (cursor.hasNext()) {
                //  System.out.println(cursor.next().toString());
                String user_friend_id = LikeTableJSONtoSenderID(cursor.next().toString());
                arkadaslist.add(user_friend_id);
            }
            mongo.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return arkadaslist;
    }

    public String GetUserName(String User_ID2) {
        String user_name = "";
        try {
            int User_ID = Integer.parseInt(User_ID2);
            // System.out.print("UserMongoURL_ID = " + User_ID);
            //by username
            ArrayList<String> arkadaslist = new ArrayList<>();
            MongoClient mongo = new MongoClient(MongoURL, MongoPort);
            DB db = mongo.getDB(MongoDBName);
            DBCollection table = db.getCollection("users");
            BasicDBObject searchQuery = new BasicDBObject();
            searchQuery.put("userid", User_ID);
            //  System.out.println(searchQuery);
            DBCursor cursor = table.find(searchQuery);

            while (cursor.hasNext()) {
                user_name = UserTableJSONtoUsername(cursor.next().toString());
            }
            mongo.close();
        } catch (Exception e) {
        }
        return user_name;
    }

    public boolean isRecordedUsername(int userid) {
        try {
//by userid
            MongoClient mongo = new MongoClient(MongoURL, MongoPort);
            DB db = mongo.getDB(MongoDBName);
            DBCollection table = db.getCollection("users");
            BasicDBObject searchQuery = new BasicDBObject();
            searchQuery.put("userid", userid);
            DBCursor cursor = table.find(searchQuery);
            while (cursor.hasNext()) {
                System.out.println(cursor.next());
                return true;
            }
            mongo.close();
        } catch (Exception e) {
        }
        return false;
    }

    public void getDBtoMongoLikes() {
        try {

            MongoClient mongo = new MongoClient(MongoURL, MongoPort);
            DB db = mongo.getDB(MongoDBName);

            DBCollection table = db.getCollection("likes");

            Connection conn = null;
            Statement st = null;
            ResultSet rs = null;

            try {
                Class.forName("com.mysql.jdbc.Driver").newInstance();
                conn = DriverManager.getConnection(dbUrl, root, password);
                st = conn.createStatement();
                rs = st.executeQuery("Select * From `liketable`");
                int sayac = 0;
                while (rs.next()) {
                    sayac++;
                    if (sayac % 1000 == 0) {
                        System.out.println("Sayac: " + sayac);
                    }
                    if (!isRecordedLike(rs.getString("photoownersender"))) {
                        BasicDBObject document = new BasicDBObject();
                        document.put("id", rs.getInt("id"));
                        document.put("photo_id", rs.getString("photo_id"));
                        document.put("owner_id", rs.getString("owner_id"));
                        document.put("sender_id", rs.getString("sender_id"));
                        document.put("photoownersender", rs.getString("photoownersender"));
                        table.insert(document);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            mongo.close();
        } catch (Exception e) {
        }
    }
}
