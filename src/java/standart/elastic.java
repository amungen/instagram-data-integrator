package standart;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author ahmet.mungen
 */


import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.StringWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import org.apache.tomcat.util.codec.binary.Base64;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

public class elastic {
    LocalVariableList lv;
    public String search(String keyword) {
        JSONObject cred = new JSONObject();
        JSONObject auth = new JSONObject();
        JSONObject parent = new JSONObject();
        cred.put("query", keyword);
        auth.put("query_string", cred);
        //  auth.put("fields", "reference");
        parent.put("query", auth);
        parent.put("size", "30");
        parent.put("from", "300");
        String category = "_search"; //or movies/movie/_search
        System.out.println("search Parent" + parent.toString());
        StringBuilder sb = new StringBuilder();
        try {
            String url = lv.getUrlserver1() + category;
            URL object = new URL(url);
            HttpURLConnection con = (HttpURLConnection) object.openConnection();
            con.setDoOutput(true);
            con.setDoInput(true);
            con.setRequestProperty("Content-Type", "application/json");
            con.setRequestProperty("Accept", "application/json");
            con.setRequestMethod("POST");
            String authString = lv.getElasticUserName() + ":" + lv.getElasticPassword();
            System.out.println("authString = " + authString);
            byte[] authEncBytes = Base64.encodeBase64(authString.getBytes());
            String authStringEnc = new String(authEncBytes);
            con.setRequestProperty("Authorization", "Basic " + authStringEnc);
            OutputStreamWriter wr = new OutputStreamWriter(con.getOutputStream());
            wr.write(parent.toString());
            wr.flush();
//display what returns the POST request

            int HttpResult = con.getResponseCode();
            if (HttpResult == HttpURLConnection.HTTP_OK) {
                BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream(), "utf-8"));
                String line = null;
                while ((line = br.readLine()) != null) {
                    sb.append(line + "\n");
                }
                br.close();
                System.out.println("search SBtoString: " + sb.toString());
            } else {
                System.out.println("search Response: " + con.getResponseMessage());
            //    ep.writeLog("error", ep.getCurrentTime() + " / elasticprocess.search(String keyword) / ElasticSearch Else Error: " + con.getResponseMessage() + " Variables: " + parent.toString());
            }
        } catch (Exception e) {
            System.out.println("Search Exception Error:" + e.getMessage());
        }
        return sb.toString();
    }

String hata;
    public void add(String articleid, String magazinename, String doid, int year, String title, String author, String reference, String text, String downloadurl) {
        try {
            JSONObject cred = new JSONObject();
            JSONObject auth = new JSONObject();
            JSONObject parent = new JSONObject();
            cred.put("magazinenamze", trcharacter(magazinename));
            cred.put("doi", trcharacter(doid));
            cred.put("year", year);
            cred.put("title", trcharacter(title));
            cred.put("authorname", trcharacter(author));
            cred.put("fileurl", trcharacter(downloadurl));
            cred.put("reference", trcharacter(reference));
            cred.put("text", trcharacter(text));
            cred.put("articleid", trcharacter(articleid));
            // System.out.println("parent. = " + cred.toString());
            String category = "/articles/article/";
            parent = cred;
            //  System.out.println("kayda girdi 0");
            // if (AddProcessCheckDB(parent.toString())) {
            //    System.out.println("kayda girdi 1");
            String url = lv.getUrlserver1() + category;
            //    System.out.println("url = " + url);
            URL object = new URL(url);
            HttpURLConnection con = (HttpURLConnection) object.openConnection();
            con.setDoOutput(true);
            con.setDoInput(true);
            con.setRequestProperty("Content-Type", "application/json");
            con.setRequestProperty("Accept", "application/json");
            con.setRequestMethod("POST");
            String authString = lv.getElasticUserName() + ":" + lv.getElasticPassword();
            byte[] authEncBytes = Base64.encodeBase64(authString.getBytes());
            String authStringEnc = new String(authEncBytes);
            con.setRequestProperty("Authorization", "Basic " + authStringEnc);
            OutputStreamWriter wr = new OutputStreamWriter(con.getOutputStream(), "UTF-8");
            wr.write(parent.toString());
            wr.flush();
            //TTEST
            //display what returns the POST request
            StringBuilder sb = new StringBuilder();
            int HttpResult = con.getResponseCode();
            //  System.out.println("HttpResult = " + HttpResult);
            //  System.out.println("HttpURLConnection.HTTP_OK = " + HttpURLConnection.HTTP_OK);
            HttpResult = HttpResult - 1;
            //  System.out.println("HttpResult = " + HttpResult);
            if (HttpResult == HttpURLConnection.HTTP_OK) {
                //     System.out.println("1");
                BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream(), "UTF-8"));
                String line = null;
                while ((line = br.readLine()) != null) {
                    hata = line.toString();
                    sb.append(line + "\n");
                    //   System.out.println("hata = " + hata);

                    System.out.println("sb = " + sb);
                    // System.out.println("2");
                }
                br.close();
                //  System.out.println("HttpSonuc:" + sb.toString());
                //   ep.writeLog("created", cred.toString());
                //  System.out.println("cccc"+cred.toString());
            } else {
                System.out.println("4");
                System.out.println("Http eLSESonuc:" + con.getResponseMessage());
                //  ep.writeLog("error", ep.getCurrentTime() + " / elasticprocess.add(String keyword, String type) / ElasticSearch Else Error: " + con.getResponseMessage() + " Variables: " + parent.toString());

            }

            // }
        } catch (Exception e) {
            //ep.writeLog("error", ep.getCurrentTime() + " / elasticprocess.add(String keyword, String type) / ElasticSearch Exception Error: " + e.getMessage() + " Variables: ");
            System.out.println("Error Elastic Yedi");
            e.printStackTrace();
        }
    }

    
    public String trcharacter(String text) {

        return text;
    }

}
