/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package standart;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author ahmet.mungen
 */
public class impact {

    public static void main(String[] args) {
        impact i = new impact();
        //  i.getReceivedLikeNumber("3332");
        //  i.AddLocalImpact("990241851", "1");
        i.AllUsersLocalImpact();
        System.out.println("LOCAL BİTTİ");
        i.AllUsersRelativeImpact();
        System.out.println("Relative 1 Bitti");
        i.AllUsersRelativeImpact();
        System.out.println("Relative 2 Bitti");
        i.AllUsersRelativeImpact();
        System.out.println("Relative 3 Bitti");
    }

    public void AddLocalImpact(String userid, String impact) {
        database db = new database();
        int count = 0;
        try {

            Connection conn = null;
            Statement st = null;
            try {
                Class.forName("com.mysql.jdbc.Driver").newInstance();
                conn = DriverManager.getConnection(db.dbUrl, db.root, db.password);
                st = conn.createStatement();
                st.execute("UPDATE `usertable` SET `impact`='" + impact + "'WHERE `userid` =" + userid);

            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("impact = " + impact);
            }
            if (st != null) {
                st.close();
            }
            if (conn != null) {
                conn.close();
            }
        } catch (Exception e) {
        }
        // 
    }

    public int getReceivedLikeNumber(String userid) {
        database db = new database();
        int count = 0;
        try {

            Connection conn = null;
            Statement st = null;
            ResultSet rs = null;
            try {
                Class.forName("com.mysql.jdbc.Driver").newInstance();
                conn = DriverManager.getConnection(db.dbUrl, db.root, db.password);
                st = conn.createStatement();
                rs = st.executeQuery("SELECT count(id) as count FROM `liketable` WHERE `sender_id` =" + userid);

                while (rs.next()) {
                    count = rs.getInt("count");

                }

            } catch (Exception e) {
                e.printStackTrace();
            }
            if (rs != null) {
                rs.close();
            }
            if (st != null) {
                st.close();
            }
            if (conn != null) {
                conn.close();
            }
        } catch (Exception e) {
        }
        return count;
    }
    //SELECT count(id) FROM `liketable` WHERE `sender_id` = 3223

    public void AllUsersLocalImpact() {
        database db = new database();
        try {

            Connection conn = null;
            Statement st = null;
            ResultSet rs = null;
            try {
                Class.forName("com.mysql.jdbc.Driver").newInstance();
                conn = DriverManager.getConnection(db.dbUrl, db.root, db.password);
                st = conn.createStatement();
                rs = st.executeQuery("SELECT * FROM `usertable` WHERE followed_by<500 AND id < 2000;");

                while (rs.next()) {
                    String userid = rs.getString("userid");
                    int followed_by = Integer.parseInt(rs.getString("followed_by"));
                    int follows = Integer.parseInt(rs.getString("follows"));
                    int media = Integer.parseInt(rs.getString("media"));
                    int likenumber = getReceivedLikeNumber(userid);

                    if (follows == 0) {
                        follows = 1;
                    }

                    double impact = (((double) likenumber / media) * ((double) followed_by / follows)) * 100;

                    if ((impact + "").equals("Infinity") || (impact + "").equals("NaN")) {
                        impact = 0;
                    }
                    AddLocalImpact(userid, impact + "");
                }

            } catch (Exception e) {
                e.printStackTrace();
                
            }
            if (rs != null) {
                rs.close();
            }
            if (st != null) {
                st.close();
            }
            if (conn != null) {
                conn.close();
            }
        } catch (Exception e) {
        }
    }

    public void AllUsersRelativeImpact() {
        database db = new database();
        try {

            Connection conn = null;
            Statement st = null;
            ResultSet rs = null;
            int sayac = 0;
            try {
                Class.forName("com.mysql.jdbc.Driver").newInstance();
                conn = DriverManager.getConnection(db.dbUrl, db.root, db.password);
                st = conn.createStatement();
                rs = st.executeQuery("SELECT userid,impact FROM `usertable` WHERE followed_by<500 AND id < 2000;");

                while (rs.next()) {
                    String userid = rs.getString("userid");
                    int impact = Integer.parseInt(rs.getString("impact"));
                    int newimpact = UsersLikedImpactAverage(userid);
                    AddLocalImpact(userid, ((impact + newimpact) / 2) + "");
                sayac++;
                if(sayac%100==0){System.out.println(sayac);}
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
            if (rs != null) {
                rs.close();
            }
            if (st != null) {
                st.close();
            }
            if (conn != null) {
                conn.close();
            }
        } catch (Exception e) {
        }
    }

    public int UsersLikedImpactAverage(String id) {
        database db = new database();
        int toplam = 0;
        int sayac = 0;
        try {

            Connection conn = null;
            Statement st = null;
            ResultSet rs = null;
            try {
                Class.forName("com.mysql.jdbc.Driver").newInstance();
                conn = DriverManager.getConnection(db.dbUrl, db.root, db.password);
                st = conn.createStatement();
                rs = st.executeQuery("SELECT DISTINCT `sender_id` FROM `liketable` WHERE `owner_id` =" + id);

                while (rs.next()) {
                    String sender_id = rs.getString("sender_id");
                    int user_impact = UsersGetImpact(sender_id);
                    if (user_impact != 0) {
                        toplam = toplam + user_impact;
                        sayac++;
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
            if (rs != null) {
                rs.close();
            }
            if (st != null) {
                st.close();
            }
            if (conn != null) {
                conn.close();
            }
        } catch (Exception e) {
        }
        int ortalama = 0;
        if (sayac != 0) {
            ortalama = toplam / sayac;
        }
        return ortalama;
    }

    public int UsersGetImpact(String userid) {
        database db = new database();
        int toplam = 0;
        int sayac = 0;
        String impact = "0";
        try {

            Connection conn = null;
            Statement st = null;
            ResultSet rs = null;
            try {
                Class.forName("com.mysql.jdbc.Driver").newInstance();
                conn = DriverManager.getConnection(db.dbUrl, db.root, db.password);
                st = conn.createStatement();
                rs = st.executeQuery("SELECT impact FROM `usertable` WHERE `userid` =" + userid);

                while (rs.next()) {
                    impact = rs.getString("impact");
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
            if (rs != null) {
                rs.close();
            }
            if (st != null) {
                st.close();
            }
            if (conn != null) {
                conn.close();
            }
        } catch (Exception e) {
        }
        return Integer.parseInt(impact);
    }

}
